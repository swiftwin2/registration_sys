import React, { Component } from 'react'
import CandidateService from './CandidateService';

 class CandidateList extends Component {
     constructor(props){
         super(props)
         this.state={
             candidates:[]
         }
     }
     componentDidMount(){
         CandidateService.getCandidate().then((res)=>
         {this.setState({candidates:res.data});
         });
        }
     
    render() {
        return (
            <div>
                <h2 className='text-center'>CandidateList</h2>
                <div className='row'>
                    <table className='table table-striped table-bordered'>
                        <thead>
                            <tr>
                                <th>Candidate FirstName</th>
                                <th>Candidate LastName</th>
                                <th>Candidate EmailId</th>
                                <th> Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        )
    }
}
export default CandidateList;